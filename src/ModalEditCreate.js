import React, { useEffect, useState } from 'react';
import { Modal, Form, Input, Button } from 'antd';

const ModalEditCreate = ({ visible, record, onCancel, onCreateOrUpdate }) => {
  const [form] = Form.useForm();

  const [prevRecord, setPrevRecord] = useState(null); // Lưu trữ bản ghi trước đó

  useEffect(() => {
    if (record && record !== prevRecord) {
      setPrevRecord(record); // Cập nhật bản ghi trước đó khi thay đổi
      form.setFieldsValue(record); // Đặt giá trị ban đầu của biểu mẫu với bản ghi hiện tại
    }
  }, [record, form]);

  const handleOk = () => {
    form
      .validateFields()
      .then((values) => {
        form.resetFields();
        onCreateOrUpdate(values);
      })
      .catch((info) => {
        console.error('Validate Failed:', info);
      });
  };

  return (
    <Modal
      visible={visible}
      title={record ? 'Edit Record' : 'Create Record'}
      onCancel={onCancel}
      footer={[
        <Button key="cancel" onClick={onCancel}>
          Cancel
        </Button>,
        <Button key="submit" type="primary" onClick={handleOk}>
          {record ? 'Update' : 'Create'}
        </Button>,
      ]}
    >
      <Form
        form={form}
        layout="vertical"
        initialValues={record}
      >
 <Form.Item name="campaignId" label="Campaign ID" rules={[{ required: true, message: 'Please enter Campaign ID' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="click" label="Click" rules={[{ required: true, message: 'Please enter Click' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="conversion" label="Conversion" rules={[{ required: true, message: 'Please enter Conversion' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="cpc" label="CPC" rules={[{ required: true, message: 'Please enter CPC' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="ctr" label="CTR" rules={[{ required: true, message: 'Please enter CTR' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="currency" label="Currency" rules={[{ required: true, message: 'Please enter Currency' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="cvr" label="CVR" rules={[{ required: true, message: 'Please enter CVR' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="date" label="Date" rules={[{ required: true, message: 'Please enter Date' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="ecpm" label="ECPM" rules={[{ required: true, message: 'Please enter ECPM' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="impression" label="Impression" rules={[{ required: true, message: 'Please enter Impression' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="ivr" label="IVR" rules={[{ required: true, message: 'Please enter IVR' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="location" label="Location" rules={[{ required: true, message: 'Please enter Location' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="offerId" label="Offer ID" rules={[{ required: true, message: 'Please enter Offer ID' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="offerName" label="Offer Name" rules={[{ required: true, message: 'Please enter Offer Name' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="offerUuid" label="Offer UUID" rules={[{ required: true, message: 'Please enter Offer UUID' }]}>
          <Input />
        </Form.Item>
        <Form.Item name="spend" label="Spend" rules={[{ required: true, message: 'Please enter Spend' }]}>
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalEditCreate;
