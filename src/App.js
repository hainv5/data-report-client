import React, { useEffect, useState } from "react";
import { Table, Spin, Alert, Button, Modal, Form, Input, message } from "antd";
import { Pagination } from "@mui/material"; // Import Pagination từ MUI
import axios from "./service/ApiConfig";
import "antd/dist/reset.css";
import "./App.css";
import ModalEditCreate from "./ModalEditCreate";
import {
  getOfferData,
  createOfferData,
  updateOfferData,
  deleteOfferData,
} from "./service/Api";

const App = () => {
  const { confirm } = Modal;
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalElements, setTotalElements] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [modalMode, setModalMode] = useState("create"); // create or edit
  const [recordToEdit, setRecordToEdit] = useState(null);
  const [form] = Form.useForm();
  const [pageSize, setPageSize] = useState(10);

  useEffect(() => {
    fetchData(currentPage);
  }, [currentPage]);

  const fetchData = async (page) => {
    setLoading(true);
    try {
      const response = await getOfferData(page, pageSize);
      setData(response.data.content);
      setTotalElements(response.data.totalElements);
    } catch (error) {
      setError("Error fetching data");
      console.error("Error fetching data:", error);
    } finally {
      setLoading(false);
    }
  };

  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };

  const handleEdit = (record) => {
    setRecordToEdit(record);
    setModalMode("edit");
    setModalVisible(true);
    form.setFieldsValue(record);
  };

  const handleDelete = async (record) => {
    confirm({
      title: `Are you sure you want to delete record (ID: ${record.id})?`,
      content: "This action cannot be undone.",
      onOk: async () => {
        try {
          await deleteOfferData(record.id);
          message.success("Delete successfully");
          fetchData(currentPage);
        } catch (error) {
          console.error("Error deleting record:", error);
          message.error("Delete failed");
        }
      },
      onCancel() {},
    });
  };

  const handleCreateOrUpdate = async (values) => {
    try {
      if (modalMode === "create") {
        await createOfferData(values);
        message.success("Create successfully");
      } else if (modalMode === "edit") {
        await updateOfferData(recordToEdit.id, values);
        message.success("Update successfully");
      }
      setModalVisible(false);
      fetchData(currentPage);
    } catch (error) {
      console.error("Error saving record:", error);
      message.error("Save failed");
    }
  };

  const handleCancel = () => {
    setModalVisible(false);
    setRecordToEdit(null); // Xóa bản ghi khi hủy
    setModalMode("create");
    form.resetFields();
  };

  const columns = [
    { title: "ID", dataIndex: "id", key: "id" },
    { title: "Campaign ID", dataIndex: "campaignId", key: "campaignId" },
    { title: "Click", dataIndex: "click", key: "click" },
    { title: "Conversion", dataIndex: "conversion", key: "conversion" },
    { title: "CPC", dataIndex: "cpc", key: "cpc" },
    { title: "CTR", dataIndex: "ctr", key: "ctr" },
    { title: "Currency", dataIndex: "currency", key: "currency" },
    { title: "CVR", dataIndex: "cvr", key: "cvr" },
    { title: "Date", dataIndex: "date", key: "date" },
    { title: "ECPM", dataIndex: "ecpm", key: "ecpm" },
    { title: "Impression", dataIndex: "impression", key: "impression" },
    { title: "IVR", dataIndex: "ivr", key: "ivr" },
    { title: "Location", dataIndex: "location", key: "location" },
    { title: "Offer ID", dataIndex: "offerId", key: "offerId" },
    { title: "Offer Name", dataIndex: "offerName", key: "offerName" },
    { title: "Offer UUID", dataIndex: "offerUuid", key: "offerUuid" },
    { title: "Spend", dataIndex: "spend", key: "spend" },
    {
      title: "Actions",
      key: "actions",
      render: (text, record) => (
        <Button.Group>
          <Button type="primary" onClick={() => handleEdit(record)}>
            Edit
          </Button>
          <Button type="danger" onClick={() => handleDelete(record)}>
            Delete
          </Button>
        </Button.Group>
      ),
    },
  ];

  if (loading) {
    return <Spin size="large" />;
  }

  if (error) {
    return <Alert message="Error" description={error} type="error" showIcon />;
  }

  return (
    <>
      <div className="container">
        <div className="table-container">
          <div className="table-title">Data Report</div>
          <div className="fixed-button-container">
            <Button type="primary" onClick={() => setModalVisible(true)}>
              Add New Data
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={data}
            pagination={false}
            rowKey="id"
          />

          <Pagination
            count={Math.ceil(totalElements / pageSize)}
            page={currentPage}
            onChange={handlePageChange}
            className="mt-4"
          />
          <ModalEditCreate
            visible={modalVisible}
            record={recordToEdit}
            onCancel={handleCancel}
            onCreateOrUpdate={handleCreateOrUpdate}
          />
        </div>
      </div>
    </>
  );
};

export default App;
