import axios from "./ApiConfig";

const getOfferData = (page, size) => {
    return axios.get('/offerDataMint', {
      params: { page, size },
    });
  };
  
  const createOfferData = (data) => {
    return axios.post('/offerDataMint', data);
  };
  
  const updateOfferData = (id, data) => {
    return axios.put(`/offerDataMint/${id}`, data);
  };
  
  const deleteOfferData = (id) => {
    return axios.delete(`/offerDataMint/${id}`);
  };
  
  export {
    getOfferData,
    createOfferData,
    updateOfferData,
    deleteOfferData,
  };