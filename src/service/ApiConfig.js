import axios from 'axios';

const apiConfig = axios.create({
  baseURL: 'http://localhost:8080/api/v1', // Thay thế bằng URL thực tế của API của bạn
  headers: {
    'Content-Type': 'application/json'
  }
});

export default apiConfig;
